from smallcalc.repl.cli import Calc
import pytest


@pytest.fixture
def c() -> Calc:
    return Calc()


def test_twice(c):
    assert c.exec("2") == 2, "First failed"
    assert c.exec("2") == 2, "Second failed"


def test_unassignment_var(c):
    assert "x" in c.exec("x")
    assert "Unassignmented" in c.exec("x")
    assert c.exec("2") == 2, "Second failed"


def test_variable(c):
    c.exec("x = 1")
    c.exec("y = x")
    assert c.exec("y") == 1
