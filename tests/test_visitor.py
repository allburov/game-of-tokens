import pytest

from smallcalc import visitor
from smallcalc.visitor import SyntaxCalcError, UnassignmentedVariable


@pytest.fixture
def v() -> visitor.Visitor:
    return visitor.Visitor()


def test_visitor_integer(v):
    ast = {"type": "integer", "value": 12}

    assert v.visit(ast) == (12, "integer")


def test_visitor_expression_sum(v):
    ast = {
        "type": "binary",
        "left": {"type": "integer", "value": 5},
        "right": {"type": "integer", "value": 4},
        "operator": {"type": "literal", "value": "+"},
    }

    assert v.visit(ast) == (9, "integer")


def test_visitor_expression_subtraction(v):
    ast = {
        "type": "binary",
        "left": {"type": "integer", "value": 5},
        "right": {"type": "integer", "value": 4},
        "operator": {"type": "literal", "value": "-"},
    }

    assert v.visit(ast) == (1, "integer")


def test_visitor_unknown(v):
    ast = {
        "type": "binary",
        "left": {"type": "integer", "value": 5},
        "right": {"type": "integer", "value": 4},
        "operator": {"type": "literal", "value": "unknown_binary_operator"},
    }
    with pytest.raises(SyntaxCalcError):
        v.visit(ast)


def test_visitor_expression_with_multiple_operations(v):
    # 3 - 4 + 200
    ast = {
        "type": "binary",
        "left": {
            "type": "binary",
            "left": {"type": "integer", "value": 3},
            "right": {"type": "integer", "value": 4},
            "operator": {"type": "literal", "value": "-"},
        },
        "right": {"type": "integer", "value": 200},
        "operator": {"type": "literal", "value": "+"},
    }
    assert v.visit(ast) == (199, "integer")


def test_visitor_term_multiplication(v):
    ast = {
        "type": "binary",
        "left": {"type": "integer", "value": 5},
        "right": {"type": "integer", "value": 4},
        "operator": {"type": "literal", "value": "*"},
    }

    assert v.visit(ast) == (20, "integer")


def test_visitor_term_division(v):
    ast = {
        "type": "binary",
        "left": {"type": "integer", "value": 11},
        "right": {"type": "integer", "value": 4},
        "operator": {"type": "literal", "value": "/"},
    }

    assert v.visit(ast) == (2, "integer")


def test_visitor_parentheses(v):
    # (2 + 3) * 4
    ast = {
        "type": "binary",
        "left": {
            "type": "binary",
            "left": {"type": "integer", "value": 2},
            "right": {"type": "integer", "value": 3},
            "operator": {"type": "literal", "value": "+"},
        },
        "right": {"type": "integer", "value": 4},
        "operator": {"type": "literal", "value": "*"},
    }
    assert v.visit(ast) == (20, "integer")


def test_visitor_unary_minus(v):
    ast = {
        "type": "unary",
        "operator": {"type": "literal", "value": "-"},
        "content": {
            "type": "binary",
            "left": {"type": "integer", "value": 2},
            "right": {"type": "integer", "value": 3},
            "operator": {"type": "literal", "value": "+"},
        },
    }

    assert v.visit(ast) == (-5, "integer")


def test_visitor_unary_plus(v):
    ast = {
        "type": "unary",
        "operator": {"type": "literal", "value": "+"},
        "content": {
            "type": "binary",
            "left": {"type": "integer", "value": 2},
            "right": {"type": "integer", "value": 3},
            "operator": {"type": "literal", "value": "+"},
        },
    }

    assert v.visit(ast) == (5, "integer")


def test_visitor_assignment(v):
    ast = {
        "type": "assignment",
        "variable": "x",
        "value": {"type": "integer", "value": 5},
    }

    assert v.visit(ast) == (None, None)
    assert v.isvariable("x") is True
    assert v.valueof("x") == 5
    assert v.typeof("x") == "integer"


def test_visitor_variable(v):
    assignment_ast = {
        "type": "assignment",
        "variable": "x",
        "value": {"type": "integer", "value": 123},
    }

    read_ast = {"type": "variable", "value": "x"}

    v.visit(assignment_ast)
    assert v.visit(read_ast) == (123, "integer")


def test_visitor_variable_before_assignment(v):
    read_ast = {"type": "variable", "value": "x"}
    with pytest.raises(UnassignmentedVariable):
        v.visit(read_ast)


def test_visitor_exponentiation(v):
    ast = {
        "type": "binary",
        "left": {"type": "integer", "value": 2},
        "right": {"type": "integer", "value": 3},
        "operator": {"type": "literal", "value": "^"},
    }

    assert v.visit(ast) == (8, "integer")


def test_visitor_float(v):
    ast = {"type": "float", "value": 12.345}

    assert v.visit(ast) == (12.345, "float")


def test_visitor_expression_sum_with_float(v):
    ast = {
        "type": "binary",
        "left": {"type": "float", "value": 5.1},
        "right": {"type": "integer", "value": 4},
        "operator": {"type": "literal", "value": "+"},
    }
    assert v.visit(ast) == (9.1, "float")
