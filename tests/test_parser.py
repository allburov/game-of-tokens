import pytest

from smallcalc import parser


@pytest.fixture
def p() -> parser.Parser:
    return parser.Parser()


def test_parse_integer(p):
    p.lexer.load("5")

    node = p.parse_line()

    assert node.asdict() == {"type": "integer", "value": 5}


def test_parse_float(p):
    p.lexer.load("5.8")

    node = p.parse_line()

    assert node.asdict() == {"type": "float", "value": 5.8}


def test_parse_expression(p):
    p.lexer.load("2+3")

    node = p.parse_line()

    assert node.asdict() == {
        "type": "binary",
        "left": {"type": "integer", "value": 2},
        "right": {"type": "integer", "value": 3},
        "operator": {"type": "literal", "value": "+"},
    }


def test_parse_expression_understands_subtraction(p):
    p.lexer.load("2-3")

    node = p.parse_line()

    assert node.asdict() == {
        "type": "binary",
        "left": {"type": "integer", "value": 2},
        "right": {"type": "integer", "value": 3},
        "operator": {"type": "literal", "value": "-"},
    }


def test_parse_expression_with_multiple_operations(p):
    p.lexer.load("2 + 3 - 4")

    node = p.parse_line()

    assert node.asdict() == {
        "type": "binary",
        "operator": {"type": "literal", "value": "-"},
        "left": {
            "type": "binary",
            "left": {"type": "integer", "value": 2},
            "right": {"type": "integer", "value": 3},
            "operator": {"type": "literal", "value": "+"},
        },
        "right": {"type": "integer", "value": 4},
    }


def test_parse_term(p):
    p.lexer.load("2 * 3")

    node = p.parse_line()

    assert node.asdict() == {
        "type": "binary",
        "left": {"type": "integer", "value": 2},
        "right": {"type": "integer", "value": 3},
        "operator": {"type": "literal", "value": "*"},
    }


def test_parse_term_with_multiple_operations(p):
    p.lexer.load("2 * 3 / 4")

    node = p.parse_line()

    assert node.asdict() == {
        "type": "binary",
        "left": {
            "type": "binary",
            "left": {"type": "integer", "value": 2},
            "right": {"type": "integer", "value": 3},
            "operator": {"type": "literal", "value": "*"},
        },
        "right": {"type": "integer", "value": 4},
        "operator": {"type": "literal", "value": "/"},
    }


def test_parse_expression_with_term(p):
    p.lexer.load("2 + 3 * 4")

    node = p.parse_line()

    assert node.asdict() == {
        "type": "binary",
        "left": {"type": "integer", "value": 2},
        "right": {
            "type": "binary",
            "left": {"type": "integer", "value": 3},
            "right": {"type": "integer", "value": 4},
            "operator": {"type": "literal", "value": "*"},
        },
        "operator": {"type": "literal", "value": "+"},
    }


def test_parse_expression_with_parentheses(p):
    p.lexer.load("(2 + 3)")

    node = p.parse_line()

    assert node.asdict() == {
        "type": "binary",
        "left": {"type": "integer", "value": 2},
        "right": {"type": "integer", "value": 3},
        "operator": {"type": "literal", "value": "+"},
    }


def test_parse_parentheses_change_priority(p):
    p.lexer.load("(2 + 3) * 4")

    node = p.parse_line()

    assert node.asdict() == {
        "type": "binary",
        "left": {
            "type": "binary",
            "left": {"type": "integer", "value": 2},
            "right": {"type": "integer", "value": 3},
            "operator": {"type": "literal", "value": "+"},
        },
        "right": {"type": "integer", "value": 4},
        "operator": {"type": "literal", "value": "*"},
    }


def test_parse_factor_supports_unary_operator(p):
    p.lexer.load("-5")

    node = p.parse_line()

    assert node.asdict() == {
        "type": "unary",
        "operator": {"type": "literal", "value": "-"},
        "content": {"type": "integer", "value": 5},
    }


def test_parse_factor_supports_negative_expressions(p):
    p.lexer.load("-(2 + 3)")

    node = p.parse_line()

    assert node.asdict() == {
        "type": "unary",
        "operator": {"type": "literal", "value": "-"},
        "content": {
            "type": "binary",
            "left": {"type": "integer", "value": 2},
            "right": {"type": "integer", "value": 3},
            "operator": {"type": "literal", "value": "+"},
        },
    }


def test_parse_two_minus(p):
    p.lexer.load("--2")

    node = p.parse_line()

    assert node.asdict() == {
        "type": "unary",
        "operator": {"type": "literal", "value": "-"},
        "content": {
            "type": "unary",
            "operator": {"type": "literal", "value": "-"},
            "content": {"type": "integer", "value": 2},
        },
    }


def test_parse_factor_supports_unary_plus(p):
    p.lexer.load("+(2 + 3)")

    node = p.parse_line()

    assert node.asdict() == {
        "type": "unary",
        "operator": {"type": "literal", "value": "+"},
        "content": {
            "type": "binary",
            "left": {"type": "integer", "value": 2},
            "right": {"type": "integer", "value": 3},
            "operator": {"type": "literal", "value": "+"},
        },
    }


def test_parse_factor_variable(p):
    p.lexer.load("somevar")

    node = p.parse_line()

    assert node.asdict() == {"type": "variable", "value": "somevar"}


def test_parse_assignment(p):
    p.lexer.load("x = 5")

    node = p.parse_line()

    assert node.asdict() == {
        "type": "assignment",
        "variable": "x",
        "value": {"type": "integer", "value": 5},
    }


def test_parse_assignment_with_expression(p):
    p.lexer.load("x = 4 * (3 + 5)")

    node = p.parse_line()

    assert node.asdict() == {
        "type": "assignment",
        "variable": "x",
        "value": {
            "type": "binary",
            "operator": {"type": "literal", "value": "*"},
            "left": {"type": "integer", "value": 4},
            "right": {
                "type": "binary",
                "operator": {"type": "literal", "value": "+"},
                "left": {"type": "integer", "value": 3},
                "right": {"type": "integer", "value": 5},
            },
        },
    }


def test_parse_line_supports_expression(p):
    p.lexer.load("2 * 3 + 4")

    node = p.parse_line()

    assert node.asdict() == {
        "type": "binary",
        "left": {
            "type": "binary",
            "left": {"type": "integer", "value": 2},
            "right": {"type": "integer", "value": 3},
            "operator": {"type": "literal", "value": "*"},
        },
        "right": {"type": "integer", "value": 4},
        "operator": {"type": "literal", "value": "+"},
    }


def test_parse_line_supports_assigment(p):
    p.lexer.load("x = 5")

    node = p.parse_line()

    assert node.asdict() == {
        "type": "assignment",
        "variable": "x",
        "value": {"type": "integer", "value": 5},
    }


def test_parse_line_supports_assigment_var(p):
    p.lexer.load("x = y")

    node = p.parse_line()

    assert node.asdict() == {
        "type": "assignment",
        "variable": "x",
        "value": {"type": "variable", "value": "y"},
    }


def test_parse_line_supports_expression_with_var(p):
    p.lexer.load("x + y")

    node = p.parse_line()

    assert node.asdict() == {
        "type": "binary",
        "left": {"type": "variable", "value": "x"},
        "right": {"type": "variable", "value": "y"},
        "operator": {"type": "literal", "value": "+"},
    }


def test_parse_exponentiation(p):
    p.lexer.load("2 ^ 3")

    node = p.parse_expression()

    assert node.asdict() == {
        "type": "binary",
        "left": {"type": "integer", "value": 2},
        "right": {"type": "integer", "value": 3},
        "operator": {"type": "literal", "value": "^"},
    }


def test_parse_exponentiation_with_other_operators(p):
    p.lexer.load("3 * 2 ^ 3")

    node = p.parse_line()

    assert node.asdict() == {
        "type": "binary",
        "operator": {"type": "literal", "value": "*"},
        "left": {"type": "integer", "value": 3},
        "right": {
            "type": "binary",
            "left": {"type": "integer", "value": 2},
            "right": {"type": "integer", "value": 3},
            "operator": {"type": "literal", "value": "^"},
        },
    }


def test_parse_exponentiation_with_parenthesis(p):
    p.lexer.load("(3 + 2) ^ 3")

    node = p.parse_line()

    assert node.asdict() == {
        "type": "binary",
        "operator": {"type": "literal", "value": "^"},
        "left": {
            "type": "binary",
            "operator": {"type": "literal", "value": "+"},
            "left": {"type": "integer", "value": 3},
            "right": {"type": "integer", "value": 2},
        },
        "right": {"type": "integer", "value": 3},
    }


def test_parse_unary_first(p):
    p.lexer.load("-2 + 3")

    node = p.parse_line()

    assert node.asdict() == {
        "type": "binary",
        "left": {
            "type": "unary",
            "operator": {"type": "literal", "value": "-"},
            "content": {"type": "integer", "value": 2},
        },
        "right": {"type": "integer", "value": 3},
        "operator": {"type": "literal", "value": "+"},
    }


def test_parse_exponentiation_with_negative_base(p):
    p.lexer.load("-2 ^ 2")

    node = p.parse_line()

    assert node.asdict() == {
        "type": "binary",
        "operator": {"type": "literal", "value": "^"},
        "left": {
            "type": "unary",
            "operator": {"type": "literal", "value": "-"},
            "content": {"type": "integer", "value": 2},
        },
        "right": {"type": "integer", "value": 2},
    }
