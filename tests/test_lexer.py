import pytest

from smallcalc import token
from smallcalc import lexer


@pytest.fixture
def l() -> lexer.Lexer:
    return lexer.Lexer()


def test_get_tokens_understands_eof(l):
    l.load("")

    assert l.get_tokens() == [token.Token(lexer.EOF)]


def test_get_token_understands_integers(l):
    l.load("3")

    assert l.get_token() == token.Token(lexer.INTEGER, "3")


def test_get_tokens_understands_integers(l):
    l.load("3")

    assert l.get_tokens() == [
        token.Token(lexer.INTEGER, "3"),
        token.Token(lexer.EOL),
        token.Token(lexer.EOF),
    ]


def test_get_tokens_understands_unspaced_sum_of_integers(l):
    l.load("3+5")

    assert l.get_tokens() == [
        token.Token(lexer.INTEGER, "3"),
        token.Token(lexer.LITERAL, "+"),
        token.Token(lexer.INTEGER, "5"),
        token.Token(lexer.EOL),
        token.Token(lexer.EOF),
    ]


def test_get_tokens_understands_multidigit_integers(l):
    l.load("356")

    assert l.get_tokens() == [
        token.Token(lexer.INTEGER, "356"),
        token.Token(lexer.EOL),
        token.Token(lexer.EOF),
    ]


def test_get_tokens_ignores_spaces(l):
    l.load("3 + 5")

    assert l.get_tokens() == [
        token.Token(lexer.INTEGER, "3"),
        token.Token(lexer.LITERAL, "+"),
        token.Token(lexer.INTEGER, "5"),
        token.Token(lexer.EOL),
        token.Token(lexer.EOF),
    ]


def test_get_tokens_understands_subtraction(l):
    l.load("3 - 5")

    assert l.get_tokens() == [
        token.Token(lexer.INTEGER, "3"),
        token.Token(lexer.LITERAL, "-"),
        token.Token(lexer.INTEGER, "5"),
        token.Token(lexer.EOL),
        token.Token(lexer.EOF),
    ]


def test_get_tokens_understands_multiple_operations(l):
    l.load("3 + 5 - 7")

    assert l.get_tokens() == [
        token.Token(lexer.INTEGER, "3"),
        token.Token(lexer.LITERAL, "+"),
        token.Token(lexer.INTEGER, "5"),
        token.Token(lexer.LITERAL, "-"),
        token.Token(lexer.INTEGER, "7"),
        token.Token(lexer.EOL),
        token.Token(lexer.EOF),
    ]


def test_lexer_can_stash_and_pop_status(l: lexer.Lexer):
    l.load("3 5")

    l.stash()
    l.get_token()
    l.pop()

    assert l.get_token() == token.Token(lexer.INTEGER, "3")


def test_lexer_can_peek_token(l):
    l.load("3 + 5")

    l.get_token()
    assert l.peek_token() == token.Token(lexer.LITERAL, "+")


def test_get_tokens_understands_multiplication(l):
    l.load("3 * 5")

    assert l.get_tokens() == [
        token.Token(lexer.INTEGER, "3"),
        token.Token(lexer.LITERAL, "*"),
        token.Token(lexer.INTEGER, "5"),
        token.Token(lexer.EOL),
        token.Token(lexer.EOF),
    ]


def test_get_tokens_understands_division(l):
    l.load("3 / 5")

    assert l.get_tokens() == [
        token.Token(lexer.INTEGER, "3"),
        token.Token(lexer.LITERAL, "/"),
        token.Token(lexer.INTEGER, "5"),
        token.Token(lexer.EOL),
        token.Token(lexer.EOF),
    ]


def test_get_tokens_understands_parentheses(l):
    l.load("3 * ( 5 + 7 )")

    assert l.get_tokens() == [
        token.Token(lexer.INTEGER, "3"),
        token.Token(lexer.LITERAL, "*"),
        token.Token(lexer.LITERAL, "("),
        token.Token(lexer.INTEGER, "5"),
        token.Token(lexer.LITERAL, "+"),
        token.Token(lexer.INTEGER, "7"),
        token.Token(lexer.LITERAL, ")"),
        token.Token(lexer.EOL),
        token.Token(lexer.EOF),
    ]


def test_get_tokens_understands_letters(l):

    l.load("somevar")

    assert l.get_tokens() == [
        token.Token(lexer.NAME, "somevar"),
        token.Token(lexer.EOL),
        token.Token(lexer.EOF),
    ]


def test_get_tokens_understands_uppercase_letters(l):
    l.load("SomeVar")

    assert l.get_tokens() == [
        token.Token(lexer.NAME, "SomeVar"),
        token.Token(lexer.EOL),
        token.Token(lexer.EOF),
    ]


def test_get_tokens_understands_names_with_underscores(l):
    l.load("some_var")

    assert l.get_tokens() == [
        token.Token(lexer.NAME, "some_var"),
        token.Token(lexer.EOL),
        token.Token(lexer.EOF),
    ]


def test_get_tokens_understands_exponentiation(l):
    l.load("2 ^ 3")

    assert l.get_tokens() == [
        token.Token(lexer.INTEGER, "2"),
        token.Token(lexer.LITERAL, "^"),
        token.Token(lexer.INTEGER, "3"),
        token.Token(lexer.EOL),
        token.Token(lexer.EOF),
    ]


def test_get_tokens_understands_floats(l):
    l.load("3.6")

    assert l.get_tokens() == [
        token.Token(lexer.FLOAT, "3.6"),
        token.Token(lexer.EOL),
        token.Token(lexer.EOF),
    ]
