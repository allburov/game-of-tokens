import dataclasses
from dataclasses import dataclass
from typing import Any, List

from smallcalc.lexer import EOL, Lexer, LITERAL, INTEGER, NAME, FLOAT

PARENTHESES_PRIORITY = {"(": -1, ")": -1}
OP_PRIORITY = {
    "+": 1,
    "-": 1,
    "*": 2,
    "/": 2,
    "^": 3,
}
LITERAL_PRIORITY = {**OP_PRIORITY, **PARENTHESES_PRIORITY}
UNARY_PRIORITY = {"+": 10, "-": 10}


@dataclass
class Node:
    type: str
    asdict = dataclasses.asdict


@dataclass
class ValueNode(Node):
    value: Any

    @classmethod
    def create(cls, value):
        raise NotImplemented()


class IntegerNode(ValueNode):
    @classmethod
    def create(cls, value):
        return cls("integer", value=int(value))


class FloatNode(ValueNode):
    @classmethod
    def create(cls, value):
        return cls("float", value=float(value))


class VariableNode(ValueNode):
    @classmethod
    def create(cls, value):
        return cls("variable", value=value)


@dataclass
class AssignmentNode(Node):
    variable: str
    value: Node

    @classmethod
    def create(cls, variable, value):
        return cls("assignment", variable, value)


class LiteralNode(ValueNode):
    @classmethod
    def create(cls, value):
        return cls("literal", value=value)

    @property
    def priority(self):
        return priority(self.value)


class UnaryLiteralNode(ValueNode):
    @classmethod
    def create(cls, value):
        return cls("literal", value=value)

    @property
    def priority(self):
        return UNARY_PRIORITY[self.value]


def priority(value):
    return LITERAL_PRIORITY[value]


@dataclass
class BinaryExpression(Node):
    left: Node
    right: Node
    operator: LiteralNode

    @classmethod
    def create(cls, *args, **kwargs):
        return cls("binary", *args, **kwargs)


@dataclass
class UnaryExpression(Node):
    content: Node
    operator: LiteralNode

    @classmethod
    def create(cls, *args, **kwargs):
        return cls("unary", *args, **kwargs)


class ParserTwoStack(object):
    def __init__(self):
        self.lexer = Lexer()

    def parse_integer(self):
        token = self.lexer.get_token()
        return IntegerNode.create(token.value)

    def parse_float(self):
        token = self.lexer.get_token()
        return FloatNode.create(token.value)

    def parse_literal(self):
        token = self.lexer.get_token()
        return LiteralNode.create(token.value)

    def parse_unary(self):
        token = self.lexer.get_token()
        return UnaryLiteralNode.create(token.value)

    def parse_var(self):
        token = self.lexer.peek_token()
        if token.type is NAME:
            self.lexer.get_token()
            return VariableNode.create(token.value)

    def parse_assignment(self):
        self.lexer.stash()
        var = self.parse_var()
        if var:
            token = self.lexer.peek_token()
            if token.type is LITERAL and token.value == "=":
                self.lexer.get_token()
                return AssignmentNode.create(var.value, self.parse_expression())
            else:
                self.lexer.pop()
                return None

    def parse_expression(self):
        _operands: List[Node] = []
        _funcs = []

        def _create_expr():
            assert len(_funcs) != 0, "Error, Functions stack is empty"
            o = _funcs.pop()

            if isinstance(o, UnaryLiteralNode):
                l = _operands.pop()
                _operands.append(UnaryExpression.create(l, o))
            else:
                r = _operands.pop()
                l = _operands.pop()
                _operands.append(BinaryExpression.create(l, r, o))

        prev_token = None
        while True:
            token = self.lexer.peek_token()

            if token.type is INTEGER:
                _operands.append(self.parse_integer())

            elif token.type is FLOAT:
                _operands.append(self.parse_float())

            elif token.type is NAME:
                _operands.append(self.parse_var())

            elif token.type is LITERAL and token.value in "()":
                oper = self.parse_literal()
                if token.value == "(":
                    # Положить ( стек
                    _funcs.append(oper)
                else:
                    # Если это ) - забирать из стека пока не встретиться )
                    while _funcs[-1].value != "(":
                        _create_expr()
                    _funcs.pop()

            elif token.type is LITERAL and token.value in LITERAL_PRIORITY:

                if prev_token is None or prev_token.value in OP_PRIORITY:
                    oper = self.parse_unary()
                    _funcs.append(oper)
                    continue
                else:
                    oper = self.parse_literal()

                if len(_funcs) > 0 and (_funcs[-1].priority >= oper.priority):
                    _create_expr()
                    _funcs.append(oper)
                else:
                    _funcs.append(oper)

            elif token.type is EOL:
                while len(_funcs) != 0:
                    _create_expr()
                assert len(_operands) == 1
                assert len(_funcs) == 0
                return _operands.pop()
            else:
                raise Exception(f"Unrecognized token: {token}")
            prev_token = token

    def parse_line(self):
        for parse in (self.parse_assignment, self.parse_expression):
            result = parse()
            if result:
                return result

    parse_factor = parse_expression


class RecursiveDescentParser(ParserTwoStack):
    # TODO: Implement
    def parse_expression(self):
        pass


Parser = ParserTwoStack
