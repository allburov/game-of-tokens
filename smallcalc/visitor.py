import operator
from typing import NamedTuple, Any


class Result(NamedTuple):
    value: Any
    type: Any


BINARY_OPERATORS = {
    "+": operator.add,
    "-": operator.sub,
    "*": operator.mul,
    "/": operator.floordiv,
    "^": operator.pow,
}

UNARY_OPERATORS = {
    "+": lambda x: x,
    "-": lambda x: -x,
}


class VisitorException(Exception):
    def __str__(self):
        return f"{self.__class__.__name__}"


class SyntaxCalcError(VisitorException):
    pass


class UnassignmentedVariable(VisitorException):
    def __init__(self, var):
        super().__init__()
        self.var = var

    def __str__(self):
        return f"{self.__class__.__name__} = {self.var}"


class Visitor:
    def __init__(self):
        self._variables = {}

    def visit(self, ast):
        vfunc = getattr(self, f'visit_{ast["type"]}')
        return vfunc(ast)

    def visit_assignment(self, ast):
        self._variables[ast["variable"]] = self.visit(ast["value"])
        return Result(None, None)

    def visit_variable(self, ast):
        if ast["value"] not in self._variables:
            raise UnassignmentedVariable(ast["value"])

        return self._variables[ast["value"]]

    @staticmethod
    def visit_number(ast):
        return Result(*operator.itemgetter("value", "type")(ast))

    visit_float = visit_number
    visit_integer = visit_number

    def visit_unary(self, ast):
        o = ast["operator"]
        c = self.visit(ast["content"])
        ovalue = o["value"]

        try:
            ofunc = UNARY_OPERATORS[ovalue]
        except KeyError:
            raise SyntaxCalcError(f"Unknown unary operator: {ovalue}")

        return Result(ofunc(c.value), c.type)

    def visit_binary(self, ast):
        o = ast["operator"]
        l = self.visit(ast["left"])
        r = self.visit(ast["right"])
        ovalue = o["value"]

        try:
            ofunc = BINARY_OPERATORS[ovalue]
        except KeyError:
            raise SyntaxCalcError(f"Unknown binary operator: {ovalue}")

        return Result(ofunc(l.value, r.value), l.type)

    def isvariable(self, name):
        return name in self._variables

    def valueof(self, name):
        return self._variables[name][0]

    def typeof(self, name):
        return self._variables[name][1]
