import re
from collections import deque
from typing import Generator, Optional

from smallcalc.token import Token

from smallcalc.buffer import TextBuffer, EOFBufferError, EOLError


class TokenType:
    pass


class EOF(TokenType):
    pass


class EOL(TokenType):
    pass


class INTEGER(TokenType):
    pattern = re.compile(r"\d+")


class FLOAT(TokenType):
    pattern = re.compile(r"\d+\.\d+")


class LITERAL(TokenType):
    pass


class NAME(TokenType):
    pattern = re.compile(r"\w*\b")


class Lexer:
    def __init__(self):
        self._buffer = TextBuffer()
        self._tokens = []
        self._pos = 0
        self._pos_stack = deque()

    def load(self, text):
        self._buffer.load(text)
        self._tokens = list(self._parse_tokens())
        self._pos = 0
        self._pos_stack = deque()

    def _parse_integer(self, char, line):
        if char.isnumeric():
            return self._parse_pattern(char, line, INTEGER)

    def _parse_float(self, char, line):
        if char.isnumeric():
            return self._parse_pattern(char, line, FLOAT)

    def _parse_name(self, char: str, line):
        if char.isalpha():
            return self._parse_pattern(char, line, NAME)

    def _parse_pattern(self, char, line, token_type):
        m = token_type.pattern.match(line, self._buffer.column)
        if m:
            chars = m.group(0)
            token = Token(token_type, chars)
            self._buffer.skip(len(chars))
            return token

    def _parse_literal(self, char, line):
        self._buffer.skip(len(char))
        return Token(LITERAL, char)

    def _parse_space(self, char, line):
        if char == " ":
            self._buffer.skip(len(char))
            return True

    def _parse_tokens(self):
        stop = False
        while not stop:
            try:
                char: str = self._buffer.current_char
                line: str = self._buffer.current_line

                for parse in (
                    self._parse_space,
                    self._parse_name,
                    self._parse_float,
                    self._parse_integer,
                    self._parse_literal,
                ):
                    token = parse(char, line)
                    if token:
                        break

                if isinstance(token, Token):
                    yield token

            except EOLError:
                yield Token(EOL)
                self._buffer.newline()
            except EOFBufferError:
                yield Token(EOF)
                stop = True

    def get_token(self):
        token = self._tokens[self._pos]
        self._pos += 1
        return token

    def get_tokens(self):
        return self._tokens

    def stash(self):
        self._pos_stack.append(self._pos)

    def pop(self):
        assert self._pos_stack, "Stack is empty. You must call stash before pop"
        self._pos = self._pos_stack.pop()

    def peek_token(self):
        self.stash()
        token = self.get_token()
        self.pop()
        return token
