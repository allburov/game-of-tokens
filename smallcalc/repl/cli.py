from smallcalc import parser
from smallcalc import visitor


class Calc:
    def __init__(self):
        self.p = parser.Parser()
        self.v = visitor.Visitor()

    def exec(self, text):
        self.p.lexer.load(text)
        node = self.p.parse_line()
        try:
            res = self.v.visit(node.asdict())
        except visitor.VisitorException as exc:
            return str(exc)
        return res.value or ""


def main():
    c = Calc()

    while True:
        try:
            text = input("smallcalc :> ")
            res = c.exec(text)
            print(res)

        except EOFError:
            print("Bye!")
            break

        if not text:
            continue


if __name__ == "__main__":
    main()
